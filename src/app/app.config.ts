import { ApplicationConfig } from '@angular/core';
import { HTTP_INTERCEPTORS, provideHttpClient, withFetch } from '@angular/common/http';
import { provideRouter } from '@angular/router';
import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { AuthInterceptor } from './interceptors/auth';

export const appConfig: ApplicationConfig = {
  providers: [
    //provideClientHydration(),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    provideRouter(routes),
    provideAnimationsAsync(), // Note: ensure multiple calls to provideAnimationsAsync() are intentional.
    provideHttpClient(withFetch()), provideAnimationsAsync() // Adds the HttpClient to your application configuration
  ],
};

