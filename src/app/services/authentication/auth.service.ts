import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, catchError, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private http = inject(HttpClient);

  login(loginForm: FormGroup): Observable<any> {

    const email = loginForm.get('email')?.value;
    const password = loginForm.get('password')?.value;
    const username = email; // Assuming username is the same as email

    if (!email || !password) {
      return throwError(() => new Error('Email and password must not be empty.'));
    }

    const loginData = { username, password, email };

    return this.http.post<any>('http://localhost:8080/api/auth/signin', loginData).pipe(
      tap(response => {
        // Assume response includes { accessToken, id, email, roles }
        localStorage.setItem('accessToken', response.accessToken);
        localStorage.setItem('userId', response.id);
        localStorage.setItem('email', response.email);
        localStorage.setItem('roles', JSON.stringify(response.roles));
      }),
      catchError(error => throwError(() => new Error('Failed to login')))
    );

  }


  signin(signInForm: FormGroup): Observable<any> {

    const email = signInForm.get('email')?.value;
    const password = signInForm.get('password')?.value;

    if (!email || !password) {
      return throwError(() => new Error('Email and password must not be empty.'));
    }

    const signInData = { email, password };

    return this.http.post('http://localhost:8080/api/auth/signup', signInData);

  }

  get accessToken(): string | null {
    return localStorage.getItem('accessToken');
  }
  
}
