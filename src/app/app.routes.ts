import { Route, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignInComponent } from './sign-in/sign-in.component';

export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'content-projection', component: LoginComponent },
    { path: 'sign-in', component: SignInComponent }, // Define a route for sign-in
    { path: '', redirectTo: '/login', pathMatch: 'full' }, // Redirect to /login
    { path: '**', redirectTo: '/login' }

    // Other routes...
  ];
