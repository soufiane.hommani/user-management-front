// auth.interceptor.ts
import { Injectable, inject } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/authentication/auth.service';

@Injectable({
  providedIn: 'root' // Make sure the interceptor is provided at the root level
})
export class AuthInterceptor implements HttpInterceptor {

    private authService = inject(AuthService);

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Get the access token from AuthService
    const accessToken = this.authService.accessToken;
    if (accessToken) {
      // Clone the request and add the authorization header with the token
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${accessToken}`
        }
      });

      console.log('req ', req)
    }
    return next.handle(req);
  }
}