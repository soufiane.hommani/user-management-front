import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule, Router  } from '@angular/router';
import { SnackbarService } from '../services/snackbar/snackbar.service';
import { AuthService } from '../services/authentication/auth.service';

@Component({
  selector: 'app-sign-in',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatInputModule,
    MatIconModule,
    HttpClientModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'] // Note: It should be styleUrls instead of styleUrl
})
export class SignInComponent {

  private formBuilder = inject(FormBuilder);
  private authService = inject(AuthService);
  private router = inject(Router);
  private snackbarService = inject(SnackbarService)


  isLoading: boolean = false;  // Tracks whether a request is in progress
  signInError: string | null = null;

  signInForm: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]]
  });

  onSignIn() {
    this.signInError = null;
    if (this.signInForm.valid) {
      this.isLoading = true;  // Start loading
      this.authService.signin(this.signInForm).subscribe({
        next: (response) => {
          console.log('Signed in successfully', response);
          if(response.message === 'User registered successfully!'){
            this.snackbarService.openSnackbar('Sign-in successful!', 'info');
            this.router.navigate(['login']);
          }
          this.signInError = null;
          this.isLoading = false;  // Stop loading on success
        },
        error: (error) => {
          console.error('Sign-in failed:', error);
          this.signInError = 'Failed to sign in. Please try again.';
          this.isLoading = false;  // Stop loading on error
        }
      });
    }
  }

 


}
